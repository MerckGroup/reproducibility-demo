#Reproducibility Demo

##General

This project demonstrates some of the principles for reproducibility presented at the Second Annual Merck Data Science & Analytics Days.

This demonstration in particular is concerned with rock climbing routes close to a particular hometown in Italy (although the location can be changed easily). It has two final output files:

*  An elevation map of the area, with climbing routes marked by circles
* A simple linear regression of route quality ratings vs:
  * elevation
  * number of quality votes

##Data

Data comes from two sources:

* The mountainproject Data API, which can be found at [here](https://mountainproject.com/data).
  * This API is available to people with accounts (which are free).
  * The API call for the demo is https://www.mountainproject.com/data/get-routes-for-lat-lon?lat=[LAT]&lon=[LONG]&maxDistance=200&maxResults=500&key=[PRIVATE KEY], where [LAT] and [LONG] are latitude and longitude and [PRIVATE KEY] is one's mountain project private key.
* SRTM 1 arc-second global elevation data, which can be found [here](http://srtm.csi.cgiar.org/srtmdata/). We downloaded the GeoTIFF 5x5 tiles corresponding to the relevant areas of the globe, corresponding to:
  * srtm\_39\_04.zip
  * srtm\_39\_05.zip
  * srtm\_40\_04.zip
  * srtm\_40\_05.zip
  * srtm\_41\_04.zip
  * srtm\_41\_05.zip

##Setup
The demo was developed on the Insight Engine from Healthcare Digital. If one has a workspace in the account with the relevant credentials exported for the reproducibility-demo account, then no additional setup is required. The demo will download the data from the appropriate s3 bucket.

Or, one can also just put the appropriate data files in data/1\_raw. If python3 and R are installed, then the rest _should_ work fine. Some adjustments may need to be made due to OS differences, installation stuff, etc. (e.g. brew vs apt-get etc)

If one would like to change the location or data files, then put these files in data/1\_raw and change src/filenames.json appropriately. 

##Run
    make run

does the following:

1. Installs R and python packages if necessary
  * R packages are installed to the default library. We use sudo rights for this.
  * We touch the file "install" at the end so that future calls to "make run" don't waste time installing. To force installation, just remove this file.
2. Downloads data files from s3 into data/1\_raw, if the folder data/1\_raw _does not exist_.
3. Turns the json route file into a csv file. (in data/2\_parsed)
4. Combines and crops the elevation files to the area around the routes. (in data/3\_merged)
5. Plots elevation and routes (in data/4\_model)
6. Runs a linear regression (in data/4\_model)

##Makefile
The demo runs off of the makefile. The makefile is in the format 

    A: B

i.e. A is run if B does not exist. In our case, A and B are directories. 

Our makefile cascades from one directory to another:

data/1\_raw -> data/2\_parsed -> data/3\_merged -> data/4\_model
