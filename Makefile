INSTALL=install
RAW=data/1_raw
PARSED=data/2_parsed
MERGED=data/3_merged
MODEL=data/4_model

RCMD=Rscript --vanilla

$(INSTALL):
	sudo apt-get install libgdal-dev
	python3 -m pip install -r install_src/python_requirements.txt
	sudo $(RCMD) install_src/install.R
	touch install

$(RAW): $(INSTALL)
	mkdir -p $@
	aws s3 cp --no-sign-request --recursive s3://reproducibility-demo/data/1_raw/ $@

$(PARSED): $(RAW)
	mkdir -p $@
	python src/1.1_parse_routes.py $< $@

$(MERGED): $(PARSED)
	mkdir -p $@
	$(RCMD) src/2.1_crop_raster.R $< $@ $(RAW)

$(MODEL): $(MERGED)
	mkdir -p $@
	$(RCMD) src/3.1_plot.R $< $@
	$(RCMD) src/3.2_model.R $< $@

run: $(MODEL)

clean:
	find data/* -type d -exec rm -rf {} +

