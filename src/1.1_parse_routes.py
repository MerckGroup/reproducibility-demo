#!/usr/bin/env python3

#Turns json route files from mountainproject into csvs.

import sys
import json
import os
import pandas as pd

#filenames
_, in_dir, out_dir = sys.argv
filenames = json.load(open("src/filenames.json"))
in_file = os.path.join(in_dir,filenames['raw']['routes'])
out_file = os.path.join(out_dir,filenames['parsed']['routes'])

routes = json.load(open(in_file))['routes']

keys = ['name','type','rating','stars','starVotes','latitude','longitude']

route_data = [{key:x[key] for key in keys} for x in routes]

df = pd.DataFrame(route_data)
df.to_csv(out_file,index = False)
